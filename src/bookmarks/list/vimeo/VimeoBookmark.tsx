import { IVimeoBookmark } from "../../bookmarks.model";

type VimeoBookmarkProps = {
  bookmark: IVimeoBookmark;
  onDeleteBookmark: (url: string) => void;
};

const VimeoBookmark = ({ bookmark, onDeleteBookmark }: VimeoBookmarkProps) => {
  function handleClick() {
    onDeleteBookmark(bookmark.url);
  }

  function formatSeconds(seconds: number): string {
    var date = new Date(1970, 0, 1);
    date.setSeconds(seconds);
    return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
  }

  return (
    <div>
      <div dangerouslySetInnerHTML={{ __html: bookmark.html }}></div>
      <ul>
        <li>Url: {bookmark.url ?? "unknown"}</li>
        <li>Title: {bookmark.title ?? "unknown"}</li>
        <li>Author: {bookmark.author ?? "unknown"}</li>
        <li>Published on Vimeo: {bookmark.publicationDate ?? "unknown"}</li>
        <li>
          Added as bookmark:{" "}
          {bookmark.bookmarkDate !== undefined
            ? bookmark.bookmarkDate.toLocaleDateString()
            : "unknown"}
        </li>
        <li>
          Duration:{" "}
          {bookmark.duration !== undefined
            ? formatSeconds(bookmark.duration)
            : "unknown"}
        </li>
      </ul>
      <button onClick={handleClick}>Delete</button>
      <div>______________</div>
    </div>
  );
};

export default VimeoBookmark;
