import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { IVimeoBookmark } from "../../bookmarks.model";
import VimeoBookmark from "./VimeoBookmark";

describe("VimeoBookmark component", () => {
  const bookmark: IVimeoBookmark = {
    type: "Vimeo",
    html: "<div>Video html</div>",
    url: "video url",
    author: "video author",
    title: "video title",
    bookmarkDate: new Date(),
    publicationDate: "video publication date",
    duration: 10,
  };

  it("should render a video, it's properties and a delete button", async () => {
    render(<VimeoBookmark bookmark={bookmark} onDeleteBookmark={() => {}} />);

    const videoElement = await screen.findByText("Video html");
    const listElement = await screen.findByRole("list");
    const listItemElements = await screen.findAllByRole("listitem");
    const buttonElement = await screen.findByRole("button");

    expect(videoElement).toBeInTheDocument();
    expect(listElement).toBeInTheDocument();
    expect(listItemElements).toHaveLength(6);
    expect(buttonElement).toBeInTheDocument();
  });

  it("should notify parent component on bookmark delete", async () => {
    const mockOnDeleteBookmark = jest.fn();
    render(
      <VimeoBookmark
        bookmark={bookmark}
        onDeleteBookmark={mockOnDeleteBookmark}
      />
    );

    const deleteButton = await screen.findByRole("button");
    userEvent.click(deleteButton);

    expect(mockOnDeleteBookmark).toHaveBeenCalledTimes(1);
    expect(mockOnDeleteBookmark).toHaveBeenNthCalledWith(1, "video url");
  });
});
