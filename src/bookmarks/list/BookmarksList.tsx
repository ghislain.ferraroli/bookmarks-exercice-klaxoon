import { Bookmarks } from "../bookmarks.model";
import FlickrBookmark from "./flickr/FlickrBookmark";
import VimeoBookmark from "./vimeo/VimeoBookmark";

type BookmarksListProps = {
  bookmarks: Bookmarks;
  onDeleteBookmark: (url: string) => void;
};

const BookmarksList = ({ bookmarks, onDeleteBookmark }: BookmarksListProps) => {
  return (
    <ul>
      {bookmarks.map((bookmark) => {
        if (bookmark.type === "Vimeo") {
          return (
            <VimeoBookmark
              key={bookmark.url}
              bookmark={bookmark}
              onDeleteBookmark={onDeleteBookmark}
            />
          );
        } else {
          return (
            <FlickrBookmark
              key={bookmark.url}
              bookmark={bookmark}
              onDeleteBookmark={onDeleteBookmark}
            />
          );
        }
      })}
    </ul>
  );
};

export default BookmarksList;
