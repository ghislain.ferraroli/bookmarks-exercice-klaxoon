import { IFlickrBookmark } from "../../bookmarks.model";

type FlickrBookmarkProps = {
  bookmark: IFlickrBookmark;
  onDeleteBookmark: (url: string) => void;
};

const FlickrBookmark = ({
  bookmark,
  onDeleteBookmark,
}: FlickrBookmarkProps) => {
  function handleClick() {
    onDeleteBookmark(bookmark.url);
  }

  return (
    <div>
      <div dangerouslySetInnerHTML={{ __html: bookmark.html }}></div>
      <ul>
        <li>Url: {bookmark.url ?? "unknown"}</li>
        <li>Title: {bookmark.title ?? "unknown"}</li>
        <li>Author: {bookmark.author ?? "unknown"}</li>
        <li>Published on Flickr: {bookmark.publicationDate ?? "unknown"}</li>
        <li>
          Added as bookmark:{" "}
          {bookmark.bookmarkDate !== undefined
            ? bookmark.bookmarkDate.toLocaleDateString()
            : "unknown"}
        </li>
        <li>Width: {bookmark.width ?? "unknown"}</li>
        <li>Height: {bookmark.height ?? "unknown"}</li>
      </ul>
      <button onClick={handleClick}>Delete</button>
      <div>______________</div>
    </div>
  );
};

export default FlickrBookmark;
