import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { IFlickrBookmark } from "../../bookmarks.model";
import FlickrBookmark from "./FlickrBookmark";

describe("FlickrBookmark component", () => {
  const bookmark: IFlickrBookmark = {
    type: "Flickr",
    html: "<div>image html</div>",
    url: "image url",
    author: "image author",
    title: "image title",
    bookmarkDate: new Date(),
    publicationDate: "image publication date",
    width: "100",
    height: "200",
  };

  it("should render an image, it's properties and a delete button", async () => {
    render(<FlickrBookmark bookmark={bookmark} onDeleteBookmark={() => {}} />);

    const videoElement = await screen.findByText("image html");
    const listElement = await screen.findByRole("list");
    const listItemElements = await screen.findAllByRole("listitem");
    const buttonElement = await screen.findByRole("button");

    expect(videoElement).toBeInTheDocument();
    expect(listElement).toBeInTheDocument();
    expect(listItemElements).toHaveLength(7);
    expect(buttonElement).toBeInTheDocument();
  });

  it("should notify parent component on bookmark delete", async () => {
    const mockOnDeleteBookmark = jest.fn();
    render(
      <FlickrBookmark
        bookmark={bookmark}
        onDeleteBookmark={mockOnDeleteBookmark}
      />
    );

    const deleteButton = await screen.findByRole("button");
    userEvent.click(deleteButton);

    expect(mockOnDeleteBookmark).toHaveBeenCalledTimes(1);
    expect(mockOnDeleteBookmark).toHaveBeenNthCalledWith(1, "image url");
  });
});
