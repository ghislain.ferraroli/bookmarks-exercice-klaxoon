import BookmarkCreate from "./create/BookmarkCreate";
import useNoembed from "./hooks/useNoembed";
import BookmarksList from "./list/BookmarksList";

const BookmarksPage = () => {
  const { bookmarks, errors, deleteBookmark, createBookmark } =
    useNoembed();
  return (
    <section>
      <BookmarkCreate
        onCreateBookmark={createBookmark}
        errors={errors}
      />
      <BookmarksList bookmarks={bookmarks} onDeleteBookmark={deleteBookmark} />
    </section>
  );
};

export default BookmarksPage;
