import { act, renderHook } from "@testing-library/react-hooks";
import useNoembed from "./useNoembed";

export const vimeoTestUrl = "https://vimeo.com/234677070";

describe("useNoembed hook", () => {
  it("should initiate with proper values", () => {
    const { result } = renderHook(() => useNoembed());

    expect(result.current.bookmarks).toEqual([]);
    expect(result.current.errors).toEqual([]);
  });

  describe("createBookmark", () => {
    it("should add bookmark to state when good url is entered", async () => {
      const fakeVimeoResponse = {
        html: "<div>Video html</div>",
        url: "video url",
        author_name: "video author",
        title: "video title",
        upload_date: "video publication date",
        duration: 10,
        provider_name: "Vimeo",
      };

      const { result } = renderHook(() => useNoembed());

      await act(async () => {
        await result.current.createBookmark(vimeoTestUrl);
      });

      expect(result.current.errors).toEqual([]);
      expect(result.current.bookmarks).toEqual([
        {
          type: "Vimeo",
          html: "<div>Video html</div>",
          url: "video url",
          author: "video author",
          title: "video title",
          bookmarkDate: expect.any(Date),
          publicationDate: "video publication date",
          duration: 10,
        },
      ]);
    });

    it("should set error to url error when wrong url is entered", async () => {
      const { result } = renderHook(() => useNoembed());

      await act(async () => {
        await result.current.createBookmark("bad url");
      });

      expect(result.current.errors).toEqual(["url"]);
    });

    it("should set error to api error when api call returns an error", async () => {
      const { result } = renderHook(() => useNoembed());

      await act(async () => {
        await result.current.createBookmark("https://vimeo.com/badUrl");
      });

      expect(result.current.errors).toEqual(["api"]);
    });
  });
});
