import { useState } from "react";
import {
  BookmarkErrors,
  Bookmarks,
  IFlickrBookmark,
  IVimeoBookmark,
} from "../bookmarks.model";

const useNoembed = () => {
  const [bookmarks, setBookmarks] = useState<Bookmarks>([]);
  const [errors, setErrors] = useState<BookmarkErrors>([]);

  function deleteBookmark(url: string) {
    setBookmarks((bookmarks) =>
      bookmarks.filter((bookmark) => bookmark.url !== url)
    );
  }

  async function createBookmark(url: string) {
    setErrors([]);

    if (bookmarks.find((bookmark) => bookmark.url === url)) {
      // Can be done: Trigger a "bookmark already exist" message
      return;
    }

    if (isValidUrl(url)) {
      try {
        const newBookmark = await fetchBookmark(url);

        setBookmarks((bookmarks) => {
          if (bookmarks.find((bookmark) => bookmark.url === url)) {
            // Can be done: Also trigger a "bookmark already exist" message
            return bookmarks;
          }
          return bookmarks.concat([newBookmark]);
        });
      } catch {
        setErrors(["api"]);
      }
    } else {
      setErrors(["url"]);
    }
  }

  function isValidUrl(url: string): boolean {
    const matchingFlickr = url.match(
      "^(http://|https://|http://www.|https://www.)flickr.com/photos/.+$"
    );
    const matchingVimeo = url.match(
      "^(http://|https://|http://www.|https://www.)vimeo.com/.+$"
    );

    return matchingFlickr !== null || matchingVimeo !== null;
  }

  function formatResponse(
    noembedResponse: any,
    type: "Vimeo" | "Flickr"
  ): IVimeoBookmark | IFlickrBookmark {
    if (type === "Vimeo") {
      return {
        type: "Vimeo",
        url: noembedResponse.url,
        html: noembedResponse.html,
        author: noembedResponse.author_name,
        title: noembedResponse.title,
        bookmarkDate: new Date(),
        publicationDate: noembedResponse.upload_date,
        duration: noembedResponse.duration,
      };
    } else {
      return {
        type: "Flickr",
        url: noembedResponse.url,
        html: noembedResponse.html,
        author: noembedResponse.author_name,
        title: noembedResponse.title,
        bookmarkDate: new Date(),
        publicationDate: noembedResponse.upload_date,
        width: noembedResponse.width,
        height: noembedResponse.height,
      };
    }
  }

  async function fetchBookmark(
    validUrl: string
  ): Promise<IFlickrBookmark | IVimeoBookmark> {
    const apiUrl = `http://noembed.com/embed?url=${validUrl}`;
    let response = await fetch(apiUrl);
    
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const noembedResponse = await response.json();
    if (
      noembedResponse.provider_name !== "Vimeo" &&
      noembedResponse.provider_name !== "Flickr"
    ) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    return formatResponse(noembedResponse, noembedResponse.provider_name);
  }

  return {
    bookmarks,
    errors,
    deleteBookmark,
    createBookmark,
  };
};

export default useNoembed;
