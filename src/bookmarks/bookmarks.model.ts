export type Bookmarks = Array<IFlickrBookmark | IVimeoBookmark>;

export type IVimeoBookmark = {
  type: "Vimeo";
  html: string;
  url: string;
  author: string;
  title: string;
  bookmarkDate: Date;
  publicationDate: string;
  duration: number;
};

export type IFlickrBookmark = {
  type: "Flickr";
  html: string;
  url: string;
  author: string;
  title: string;
  bookmarkDate: Date;
  publicationDate: string;
  width: string;
  height: string;
};

export type BookmarkErrors = Array<"api" | "url">;
