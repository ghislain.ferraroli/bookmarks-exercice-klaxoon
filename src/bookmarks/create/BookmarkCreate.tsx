import { useState } from "react";
import { BookmarkErrors } from "../bookmarks.model";

type BookmarkCreateProps = {
  onCreateBookmark: (url: string) => void;
  errors: BookmarkErrors;
};

const BookmarkCreate = ({ onCreateBookmark, errors }: BookmarkCreateProps) => {
  const [url, setUrl] = useState("");

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    onCreateBookmark(url);
    setUrl("");
    event.preventDefault();
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Url:
        <input
          type="text"
          style={{
            borderColor: errors.length > 0 ? "red" : undefined,
          }}
          name="url"
          onChange={(e) => setUrl(e.target.value)}
          value={url}
        />
      </label>
      <input type="submit" value="Add" />

      {errors.map((error) => {
        return (
          <div style={{ color: "red" }}>
            {error === "api"
              ? "An error has occured when trying to request this url"
              : "Invalid url"}
          </div>
        );
      })}
    </form>
  );
};

export default BookmarkCreate;
