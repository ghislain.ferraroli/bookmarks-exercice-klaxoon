import { rest } from "msw";
import { vimeoTestUrl } from "../bookmarks/hooks/useNoembed.test";

export const handlers = [
  rest.get(`http://noembed.com/embed`, (req, res, ctx) => {
    const bookmarkResponse = {
      html: "<div>Video html</div>",
      url: "video url",
      author_name: "video author",
      title: "video title",
      upload_date: "video publication date",
      duration: 10,
      provider_name: "Vimeo",
    };
    const urlParam = req.url.searchParams.get("url");
    if (urlParam === vimeoTestUrl) {
      return res(ctx.status(200), ctx.json(bookmarkResponse));
    } else {
      return res(ctx.status(400), ctx.json({ errorMessage: "Bad Request" }));
    }
  }),
];
